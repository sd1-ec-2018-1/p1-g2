// Load the TCP Library
net = require('net');

// Load the HashSet library for the 'mode' set
HashSet = require('hashset');

//Load the RFC commands
const rfc31 = require('./RFC-3.1-irc-server');
const rfc32 = require('./RFC-3.2-irc-server');
const rfc33 = require('./RFC-3.3-irc-server');
const rfc34 = require('./RFC-3.4-irc-server');
const rfc36 = require('./RFC-3.6-irc-server');
const rfc37 = require('./RFC-3.7-irc-server');
/*
Introduce new global variables and arrays in the main thread
*/

var global = {

	//Keep track of time
	'hms' : function(){
		//Keep track of time
		var d = new Date();
		var h = d.getHours();
		var m = d.getMinutes();
		var s = d.getSeconds();
		if(s<10){
			s = '0'+s;
		}
		if(m<10){
			m = '0'+m;
		}
		
		return '['+h+":"+m+":"+s+']';
	},

<<<<<<< HEAD
=======
    'USERS' : {}, 
>>>>>>> 4dc353c2475ebcfa963cf1037a5de6bf6e1a570e
	//Keep track of the chat clients
	'clients' : [],
	
	//Map of nicks
	'nicks' : {},
	
	// channel list
	'channels' : {},
	//Map of users
	'usernames' : {},
	'realnames' : {},

	//Map of passwords
	'hashedPasswords' : {},

	//Operator password for shifting to operator mode with the OPER command
	'operatorpassword' : "senhadeoperador",

	//Available modes
	'availablemodes' : {
		'o': 'operator',
		'a': 'away',
		'i': 'invisible',
		'w': 'wallop reciever',
		'r': 'restricted user connection',
		'0': 'local operator',
		//'s': 'receipt of server notices'//Obsolete, but can still be implemented

	},

	//Map of services
	'services' : {
		//'name' : service {}
	},
	
	//Model for creating new services
	'service' : function (distribution, type, info){
		this.distribution = distribution;
		this.type = type;
		this.info = info;
	},
	
	//Message of the day
	'motd' : '*MENSAGEM MOTIVACIONAL DO DIA PARA TE FAZER MAIS FELIZ, MESMO QUE SUA VIDA SEJA UM LIXO*',
	
	//Nodejs Version
	'version': 'v8.4.0',	//MELHORAR ISTO DEPOIS(CRIAR MÉTODO PARA LER A VERSÃO)

  //Let's count how many times each command has been used
	'counter' : {
		'NICK' :0,
		'USER' :0,
		'PASS' :0,
		'OPER' :0,
		'MODE' :0,
		'QUIT' :0,
		'SERVICE' :0,
		'SQUIT' :0,
		'AWAY' :0,
		'PRIVMSG' :0,
		'NOTICE' :0,
		'TIME' :0,
		'MOTD' :0,
		'LUSERS' :0,
		'VERSION' :0,
		'STATS' :0,
		'INFO' :0,
		'JOIN' :0,
		'PART' :0,
	},
	
	//Map of servers
	/*'servers': {
		//'www.serverexample.com' : ['client1', 'client2']	//DELETE AFTER TESTING
		//every client should also have an associated object socket.servers = []
	},
	THERE WILL ONLY BE OPERATIONS IN A SINGLE SERVER*/
}
Array.prototype.GLOBAL  = global;

// Start a TCP Server
net.createServer(function (socket) {

  //Keep track of the channels the user is on
  socket.channels = [];

  // Send a message to all clients
  broadcast = function (message, sender) {
    global.clients.forEach(function (client) {
      // Don't want to send it to sender
      if (client === sender) return;
      client.write(message);
    });
    // Log it to the server output too
    process.stdout.write(message)
  }

  // Identify this client
  socket.name = socket.remoteAddress + ":" + socket.remotePort 

  // Put this new client in the list
  global.clients.push(socket);

  //Mode set for each client (does not allow repetition of terms)
  socketmode = new HashSet();
  socket.mode = socketmode;

  // Send a nice welcome message and announce
  socket.write("Welcome " + socket.name + "\n");
  broadcast(socket.name + " joined the chat\n", socket);

  // Handle incoming messages from clients.
  socket.on('data', function (data) {
    // broadcast(socket.name + "> " + data, socket);
    analisar(data);
  });

  //In case the disconnection occurred with the QUIT command
  socket.on('quit', function () {
    broadcast(socket.username + ": QUIT : "+ socket.quitmsg+'\n', socket);
    socket.end();
  });
  // Remove the client from the list when it leaves
  socket.on('end', function () {
    if(socket.isgone){return; }
    broadcast(socket.name + " left the chat.\n");
    global.clients.splice(global.clients.indexOf(socket), 1);//ELIMINAR OUTRAS PROPRIEDADES DE USER FANTASMA POS DELETE
	for (property in socket){
		delete socket.property;
	}
    socket.isgone = true;
  });

  function analisar(data) {
    // visualizar os dados como são (sem tratamento)
    // console.log(data);
    // visualizar como string
    // console.log(String(data));
    // visualizar os caracteres especiais enviados
    // console.log(JSON.stringify(String(data)));
    // o método trim remove os caracteres especiais do final da string
    let mensagem = String(data).trim();
    // o método split quebra a mensagem em partes separadas p/ espaço em branco
    var args = mensagem.split(" "); 
    var args1 = args[0].toUpperCase(); //Uppercases the command word
    switch (args1){
	/*
	Seguindo os moldes do grupo 5, utilizamos o require('arquivo');
	*/

	/*
	Use the following model to introduce new functions:
	case "FUNCTION"
		rfcXX.function(args, socket, global);
	break;
	*/
	case "NICK":
		global.counter[args[0]]++;
		rfc31.nick(args, socket, global);
	break;
	case "USER":
		global.counter[args[0]]++;
		rfc31.user(args, socket, global);
	break;
	case "PASS":
		global.counter[args[0]]++;
		rfc31.pass(args, socket, global);
	break;
	case "OPER":
		global.counter[args[0]]++;
		rfc31.oper(args, socket, global);
	break;
	case "MODE":
		global.counter[args[0]]++;
		rfc31.mode(args, socket, global);
	break;
	case "QUIT":
		global.counter[args[0]]++;
		rfc31.quit(args, socket, global);
	break;
	case "SERVICE":
		global.counter[args[0]]++;
		rfc31.service(args, socket, global);
	break;
	case "SQUIT":
		global.counter[args[0]]++;
		rfc31.squit(args, socket, global);
	break;
	case "AWAY":
		global.counter[args[0]]++;
		rfc31.away(args, socket, global);
	break;
	case "PRIVMSG":
		global.counter[args[0]]++;
		rfc33.privmsg(args, socket, global);
	break;
	case "NOTICE":
		global.counter[args[0]]++;
		rfc33.notice(args, socket, global);
	break;
	case "TIME":
		global.counter[args[0]]++;
		rfc34.time(args, socket, global);
	break;
	case "MOTD":
		global.counter[args[0]]++;
		rfc34.motd(args, socket, global);
	break;
	case "LUSERS":
		global.counter[args[0]]++;
		rfc34.lusers(args, socket, global);
	break;
	case "VERSION":
		global.counter[args[0]]++;
		rfc34.version(args, socket, global);
	break;
	case "STATS":
		global.counter[args[0]]++;
		rfc34.stats(args, socket, global);
	break;
	case "JOIN":
		global.counter[args[0]]++;
		rfc32.join(args, socket, global);
	break;
	case "PART":
		global.counter[args[0]]++;
		rfc32.part(args, socket, global);
	break;
	case "TOPIC":
		rfc32.topic(args,socket,global);
	break; 
	default:
	socket.write("ERRO: Comando inválido\n");
    }
  }

socket.on('confirmation-expired', function(){
	//once the password has been used for a command that requires it, it should be requested again
	socket.passconfirm = false;
});

}).listen(6667);

// Put a friendly message on the terminal of the server.
console.log("Chat server running at port 6667\n");
